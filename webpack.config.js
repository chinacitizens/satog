const path = require('path');
const dotenv = require('dotenv');
const webpack = require('webpack');
const dateFormat = require('dateformat');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpegPlugin = require('imagemin-mozjpeg');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// common config
let config = {
    entry: ['@babel/polyfill', path.join(__dirname, '/src/index.js')],
    output: {
        path: path.join(__dirname, '/build'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        symlinks: false,
        cacheWithContext: false
    },
    optimization: {
        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false
    }
};

module.exports = (env, opts) => {
    // common vars
    let devMode = opts.mode !== 'production';
    let fileEnv = dotenv.config().parsed;
    let envKeys = Object.keys(fileEnv).reduce((prev, next) => {
        prev[`env.${next}`] = JSON.stringify(fileEnv[next]);
        return prev;
    }, {});
    let hash = '[hash:6]'+ dateFormat(new Date(), 'ddmmyyHH');
    let root = path.join(__dirname, process.env.APP_ROOT);

    // common modules
    config.module = {
        rules: [
            {
                test: /\.mjs$/,
                include: path.resolve(__dirname, 'node_modules'),
                type: 'javascript/auto'
            },
            {
                test: /\.js|jsx/,
                exclude: /node_modules/,
                include: path.resolve(__dirname, 'src'),
                loader: 'babel-loader',
                query: {
                    presets:['@babel/env','@babel/react'],
                    plugins: [
                        "@babel/plugin-proposal-class-properties"
                    ]
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: false
                        }
                    },
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico)$/,
                loader: 'file-loader',
                options: {
                    name: devMode ? 'images/[name].[ext]' : 'images/'+ hash +'.[ext]'
                }
            },
            {
                test: /\.(woff(2)?|ttf|eot)$/,
                include: path.resolve(__dirname, 'src'),
                loader: 'file-loader',
                options: {
                    name: devMode ? 'fonts/[name].[ext]' : 'fonts/'+ hash +'.[ext]'
                }
            }
        ]
    };

    // common plugins
    config.plugins = [
        new webpack.DefinePlugin(envKeys),
        new HtmlWebpackPlugin({
            template: root + '/index.html',
            filename: 'index.html',
            minify: {
                collapseWhitespace: true
            },
            title: process.env.APP_NAME,
            meta: {
                viewport: 'width=device-width, initial-scale=1.0, shrink-to-fit=no'
            },
            favicon: root + '/favicon.ico'
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].min.css'
        }),
        new CleanWebpackPlugin({
            dry: false,
            cleanOnceBeforeBuildPatterns : ['./', '../var/cache/*'],
            dangerouslyAllowCleanPatternsOutsideProject: true
        })
    ];

    // development
    if (devMode) {
        config.output.filename = 'js/[name].min.js';

        config.devServer = {
            host: '0.0.0.0',
            contentBase: path.join(__dirname, '/build'),
            compress: true,
            disableHostCheck: true,
            historyApiFallback: true,
            hot: true,
            watchOptions: {
                poll: true
            },
            proxy: {
                '/api/*': {
                    target: 'http://'+ process.env.API_SERVER_HOST +':'+ process.env.API_SERVER_PORT,
                    secure: false
                }
            }
        };

        return config;

    // production
    } else {
        config.output.filename = 'js/'+ hash +'.min.js';

        // production plugins
        config.plugins = [
            new webpack.DefinePlugin(envKeys),
            new CopyWebpackPlugin([
                {
                    from: root + '/.htaccess',
                    to: './'
                }
            ]),
            new HtmlWebpackPlugin({
                template: root + '/index.html',
                filename: 'index.html',
                minify: {
                    collapseWhitespace: true
                },
                title: process.env.APP_NAME,
                meta: {
                    viewport: 'width=device-width, initial-scale=1.0, shrink-to-fit=no'
                },
                favicon: root + '/favicon.ico'
            }),
            new MiniCssExtractPlugin({
                filename: 'css/'+ hash +'.min.css'
            }),
            new OptimizeCSSAssetsPlugin(),
            new UglifyJsPlugin({
                extractComments: {
                    condition: true,
                    filename(file) {
                        return `${file}.LICENSE`;
                    },
                    banner(licenseFile) {
                        return `License information can be found in ${licenseFile}`;
                    }
                }
            }),
            new CleanWebpackPlugin({
                dry: false,
                cleanOnceBeforeBuildPatterns : ['./', '../var/cache/*'],
                dangerouslyAllowCleanPatternsOutsideProject: true
            }),
            new ImageminPlugin({
                test: /\.(png|jpe?g|gif|svg|ico)$/,
                minFileSize: 10000,
                pngquant: ({
                    quality: '50'
                }),
                plugins: [
                    ImageminMozjpegPlugin({
                        quality: '50'
                    })
                ],
                cacheFolder: path.join(__dirname, '/var/cache/prod/images')
            }),
            new CompressionPlugin({
                test: /\.js$|\.css$|\.html$/,
                cache: path.join(__dirname, '/var/cache/prod')
            })
        ];

        return config;
    }
};