FROM node:alpine

RUN apk --update add \
    yarn \
    automake \
    autoconf \
    build-base \
    zlib \
    libpng \
    libjpeg-turbo \
&& rm -rf /var/cache/apk/* \