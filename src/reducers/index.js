import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {intlReducer} from 'react-intl-redux';
import {reducer as fetchReducer} from 'react-redux-fetch';
import routes from '../config/routes';
import messages from '../translations';

const navigatorLanguage = navigator.language.substring(0, 2);

export const initialState = {
    root: {
        routes: routes,
        locales: env.APP_LOCALES.split('|'),
        isModalOpen: false,
        route: 'home',

    },
    intl: {
        locale: navigatorLanguage,
        defaultLocale: navigatorLanguage,
        messages: messages[navigatorLanguage],
    }
};

const rootReducer = (state = initialState, action) => {
    let html = document.getElementsByTagName('html')[0];
    let root = document.getElementById('root');

    switch (action.type) {
        case '@@INIT':
            let initialRoute = getRouteName(location.pathname);
            html.setAttribute('lang', navigatorLanguage);

            return {...state, route: initialRoute};
        case '@@router/LOCATION_CHANGE':
            let changedRoute = getRouteName(action.payload.location.pathname);
            root.dataset.route = changedRoute;

            return {...state, route: changedRoute};
        case '@@intl/UPDATE':
            html.setAttribute('lang', action.payload.locale);

            return state;
        case 'TOGGLE_MODAL':
            root.classList.toggle('no-scroll');

            return {...state, isModalOpen: action.payload};
        default:
            return state;
    }
};

function getRouteName(pathname) {
    let route = initialState.root.routes.filter(route => {
        return route.path === pathname;
    });

    return route.length !== 0 ? route[0].name : 'error';
}

export const reducers = (browserHistory) => combineReducers({
    'root': rootReducer,
    'intl': intlReducer,
    'router': connectRouter(browserHistory),
    'repository': fetchReducer
});