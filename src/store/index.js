import {applyMiddleware, compose, createStore} from 'redux';
import {routerMiddleware} from 'connected-react-router';
import {reducers, initialState} from '../reducers';
import {createBrowserHistory} from 'history';
import {middleware as fetchMiddleware} from 'react-redux-fetch';

export const browserHistory = createBrowserHistory();

export const store = createStore(
    reducers(browserHistory),
    initialState,
    compose(
        applyMiddleware(
            routerMiddleware(browserHistory), // for dispatching history actions
            fetchMiddleware // for dispatching fetch actions
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // for redux devtool (chrome)
    )
);


