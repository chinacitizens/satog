import {library} from '@fortawesome/fontawesome-svg-core';
import {faChevronDown, faTimesCircle} from '@fortawesome/free-solid-svg-icons';

library.add(
    faChevronDown,
    faTimesCircle
);