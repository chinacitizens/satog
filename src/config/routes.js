import HomeComponent from '../components/views/home/Home';
import DashboardComponent from '../components/views/admin/dashboard/Dashboard';
import AboutComponent from '../components/views/about/About';

export default [
    {
        path: '/',
        name: 'home',
        exact: true,
        component: HomeComponent
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: DashboardComponent
    },
    {
        path: '/about',
        name: 'about',
        component: AboutComponent
    },
];