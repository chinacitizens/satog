import {TOGGLE_MODAL} from '../constants/action-types';
import {updateIntl} from 'react-intl-redux';
import messages from '../translations';

export const toggleModal = ({dispatch}) => {
    return (isOpen) => {
        dispatch({
            type: TOGGLE_MODAL,
            payload: isOpen
        });
    };
};

export const changeLocale = ({dispatch}) => {
    return (nextLocale) => {
        dispatch(updateIntl({
            locale: nextLocale,
            messages: messages[nextLocale],
        }));
    };
};
