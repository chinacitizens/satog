import React, {Component} from 'react';
import {FormattedMessage as Message} from 'react-intl';
import {connect} from 'react-redux';
import {toggleModal} from '../../../actions';
import MemberComponent from '../../fetched/member/Member';
import './home.scss';

const mapDispatchToProps = (dispatch) => {
    return {
        toggleModal: toggleModal({dispatch})
    };
};

class Home extends Component {
    render = () => {
        return (
            <section>
                <h1><Message id="home.title.greeting" /></h1>
                <MemberComponent/>
                <button onClick={() => this.props.toggleModal(true)}>show modal</button>
            </section>
        );
    }
}

export default connect(null, mapDispatchToProps)(Home);