import React, {Component} from 'react';
import {connect} from 'react-redux';
import {toggleModal} from '../../../../actions/index';
import './dashboard.scss';

const mapDispatchToProps = (dispatch) => {
    return {
        toggleModal: toggleModal({dispatch})
    };
};

class Dashboard extends Component {
    render = () => {
        return (
            <section>
                <h1>Dashboard</h1>
                <button onClick={() => this.props.toggleModal(true)}>show modal</button>
            </section>
        );
    }
}

export default connect(null, mapDispatchToProps)(Dashboard);