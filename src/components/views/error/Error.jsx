import React, {Component} from 'react';
import './error.scss';

class Error extends Component {
    render = () => {
        return (
            <section>
                <h1>Not found 404</h1>
            </section>
        );
    }
}

export default Error;