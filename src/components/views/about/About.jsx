import React, {Component} from 'react';
import {connect} from 'react-redux';
import {toggleModal} from '../../../actions';
import './about.scss';

const mapDispatchToProps = (dispatch) => {
    return {
        toggleModal: toggleModal({dispatch})
    };
};

class About extends Component {
    render = () => {
        return (
            <section>
                <h1>About</h1>
                <button onClick={() => this.props.toggleModal(true)}>show modal</button>
            </section>
        );
    }
}

export default connect(null, mapDispatchToProps)(About);