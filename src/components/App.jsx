require('../config/packages/font-awesome');

import React, {Component} from 'react';
import {ConnectedRouter} from 'connected-react-router';
import PageComponent from './page/Page';
import './app.scss';

class App extends Component {
    render = () => {
        return (
            <ConnectedRouter history={this.props.history}>
                <PageComponent/>
            </ConnectedRouter>
        );
    }
}

export default App;