import React, {Component}from 'react';
import {connect} from 'react-redux';
import {toggleModal} from '../../actions';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './modal.scss';

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.root.isModalOpen,
        route: state.root.route
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleModal: toggleModal({dispatch})
    };
};

class Modal extends Component {
    render = () => {
        return (
            <div className={this.props.isModalOpen ? 'show modal' : 'hide modal'}>
                <div className="modal-dialog">
                    <div className="modal-close" onClick={() => this.props.toggleModal(false)}>
                        <FontAwesomeIcon icon="times-circle" />
                    </div>

                    <div className="modal-header">
                        <h1>My modal title</h1>
                    </div>

                    <div className="modal-body"> </div>
                    <div className="modal-footer"> </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal);