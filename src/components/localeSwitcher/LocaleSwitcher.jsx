import React, {Component} from 'react';
import Select from 'react-select';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {connect} from 'react-redux';
import FlagIconFactory from 'react-flag-icon-css';
import {changeLocale} from '../../actions';

const FlagIcon = FlagIconFactory(React, { useCssModules: false });

const mapStateToProps = (state) => {
    return {
        intl: state.intl,
        root: state.root
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeLocale: changeLocale({dispatch})
    };
};

class LocaleSwitcher extends Component {
    getOption(locale) {
        return {
            value: locale,
            label: locale === 'en' ? <FlagIcon code={'gb'} /> : <FlagIcon code={locale} />
        };
    }

    getOptions(locales) {
        return locales.map((locale, i) => {
            return this.getOption(locale);
        });
    }

    render = () => {
        return (
            <Select
                components={<FontAwesomeIcon icon="chevron-down" />}
                options={this.getOptions(this.props.root.locales)}
                defaultValue={this.getOption(this.props.intl.locale)}
                onChange={option => this.props.changeLocale(option.value)}
                isSearchable={false}
            />
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocaleSwitcher);