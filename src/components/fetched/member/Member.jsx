import React, {Component} from 'react';
import connect from 'react-redux-fetch';

const fetchConfig = [
    {
        resource: 'Data', // resource name props => breweries+Fetch, dispatch+Pokemon+Method
        method: 'GET', // You can omit this, this is the default
        request: {
            url: 'http://localhost:82/api/core/test',
            headers: {
                'Accept': 'application/json'
            }
        },
    }
];

const mapStateToProps = (state) => {
    return {
        repository: state.repository
    };
};

class Member extends Component {
    componentDidMount() {
        this.props.dispatchDataGet();
    }

    render = () => {
        const data = this.props.DataFetch;

        if (data.rejected) {
            return <div>Oops... Could not fetch data!</div>;
        }

        if (data.fulfilled) {
            return (
                <ul>
                    {data.value.map(item => (
                        <li key={item.id}>{item.id}</li>
                    ))}
                </ul>
            );
        }

        return <div>Loading data...</div>;
    }
}

export default connect(fetchConfig, mapStateToProps)(Member);