import React, {Component, Fragment} from 'react';
import {FormattedMessage as Message} from 'react-intl';
import {connect} from 'react-redux';
import {Route, Switch, Link} from 'react-router-dom';
import ModalComponent from '../modal/Modal';
import ErrorComponent from '../views/error/Error';
import LocaleSwitcherComponent from '../localeSwitcher/LocaleSwitcher';
import './page.scss';

const mapStateToProps = (state) => {
    return {
        routes: state.root.routes
    };
};

class Page extends Component {
    render = () => {
        return (
            <Fragment>
                <header className="main-header">
                    <nav>
                        <ul>
                            <li>
                                <Link to={'/'}>
                                    {<Message id="header.nav.home" />}
                                </Link>
                            </li>
                            <li>
                                <Link to={'/dashboard'}>
                                    {<Message id="header.nav.dashboard" />}
                                </Link>
                            </li>
                            <li>
                                <LocaleSwitcherComponent/>
                            </li>
                        </ul>
                    </nav>
                </header>

                <main className="main-content">
                    <Switch>
                        {this.props.routes.map((route, i) => (
                            <Route
                                key={i}
                                path={route.path}
                                exact={route.exact}
                                render={props => (
                                    <route.component {...props} />
                                )}
                            />
                        ))}
                        <Route component={ErrorComponent} />
                    </Switch>
                </main>

                <footer className="main-footer">
                    <nav>
                        <ul>
                            <li>
                                <Link to={'/about'} title={<Message id="footer.nav.about" />}>
                                    {<Message id="footer.nav.about" />}
                                </Link>
                            </li>
                        </ul>
                    </nav>
                </footer>

                <ModalComponent/>
            </Fragment>
        );
    }
}

export default connect(mapStateToProps)(Page);