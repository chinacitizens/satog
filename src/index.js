import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-intl-redux';
import {store, browserHistory} from './store';
import App from './components/App';

ReactDOM.render(
    <Provider store={store}>
        <App history={browserHistory}/>
    </Provider>,
    document.getElementById('root')
);