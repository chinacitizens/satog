# SATOG v1-alpha

#### PREREQUESITE

- docker
- make (mingw on windows)
- git
- ngrok
- Redux DevTools (Browser extension) (facultative)

#### PROJECT INSTALLATION

- clone project
- set or create '.env' file variables at project root (ex: .env.default)
- run: make
- go to http://localhost:85 for Jenkins interface

#### BUILD FOR PROD

- wait until node_modules/.bin folder appears
- run: make build
- go to http://localhost

#### START DEV SERVER

- wait until node_modules/.bin folder appears
- run: make dev
- go to http://localhost:81

#### DEPENDENCIES INSTALLATION

- run: make node
- run: yarn install ...

#### JENKINS INSTALLATION

- run: make jenkins
- run: cd var/jenkins_home/secrets
- run: cat initialAdminPassword
- copy password
- go to http://localhost:85 and paste
- install recommanded plugins
- create user
- go to http://localhost:85/pluginManager/ and download and install bitbucket plugin
- restart jenkins (refresh the page if its taking too long)
- create the first job/item :
    - name : satog-dev
    - type : freestyle project
    - repository : https://Bitbucket_Username@bitbucket.org/chinacitizens/satog.git
    - branch : dev
    - checkbox: 'Build when a change is pushed to BitBucket' 
    - save and launch first build
- create the second job/item :
    - name : satog-staging
    - type : freestyle project
    - repository : https://Bitbucket_Username@bitbucket.org/chinacitizens/satog.git
    - branch : staging
    - checkbox: 'Build when a change is pushed to BitBucket' 
    - save and launch first build
- create the third job/item :
    - name : satog-prod
    - type : freestyle project
    - repository : https://Bitbucket_Username@bitbucket.org/chinacitizens/satog.git
    - branch : master
    - checkbox: 'Build when a change is pushed to BitBucket' 
    - save and launch first build
    
### CODING PROCESS
(To do everytime the computer or ngrok restarts !!)

- start containers
- run: ngrok http 85
- go to : https://bitbucket.org/chinacitizens/satog/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin
- add new webhook :
    - title : satog-jenkins
    - URL : http://{HASH.ngrok.io}/bitbucket-hook/
    - checkbox : Skip certificate verification
- start coding
- write unit tests
- merge your branch on dev and go to http://localhost:85/job/satog-dev to check the tests results for any regression
- correct bugs on your branch (if any) and merge again on dev

#### TOOLS

See Makefile
