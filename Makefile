include .env

# build and mount docker containers
start:
	docker-compose up -d --build && docker image prune -a -f

# install project dependencies
install:
	docker exec -ti $(APP_NAME)_node_container sh -c "yarn install"

# update project dependencies
update:
	docker exec -ti $(APP_NAME)_node_container sh -c "yarn update"

# build resources
build:
	docker exec -ti $(APP_NAME)_node_container sh -c "yarn run build"

# run development server
dev:
	docker exec -ti $(APP_NAME)_node_container sh -c "yarn run dev"

# enter nginx container
nginx:
	docker exec -ti $(APP_NAME)_nginx_container sh

# enter jenkins container
jenkins:
	docker exec -ti ${APP_NAME}_jenkins_container sh

# enter node container
node:
	docker exec -ti ${APP_NAME}_node_container sh

# stop all docker containers
stop:
	docker kill $(shell docker ps -aq)

# remove all docker containers
rem-c:
	docker rm $(shell docker ps -aq)

# remove all docker images
rem-i:
	docker rmi $(shell docker images -q)

# remove all docker containers and images
boom:
	make rem-c && make rem-i
